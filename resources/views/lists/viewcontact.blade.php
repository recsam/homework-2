<table class="table table-bordered">
    <tbody>
    <tr>
        <th style="width: 10rem">Name</th>
        <td>{{ $contact_information->name }}</td>
    </tr>
    <tr>
        <th style="width: 10rem">Email</th>
        <td>{{ $contact_information->email }}</td>
    </tr>
    <tr>
        <th style="width: 10rem">Phone Number</th>
        <td>{{ $contact_information->phone_number }}</td>
    </tr>
    <tr>
        <th style="width: 10rem">Subject</th>
        <td>{{ $contact_information->subject_name }}</td>
    </tr>
    <tr style="height: 15rem">
        <th style="width: 10rem">Message</th>
        <td>{!! $contact_information->message !!}</td>
    </tr>
    </tbody>
</table>
