@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact Submitted')

@section('body')
    <div class="d-flex flex-column align-items-center">
        <div class="text-center my-5">
            <h3>Your message have been submitted to our system!</h3>
            <p>Our team will feedback to you soon, Thank you</p>
        </div>
        <a href="{{ route('contacts.create') }}" class="text-uppercase btn btn-outline-secondary"><i class="fa fa-chevron-left"></i> Go Back Home Page</a>
    </div>
@stop
