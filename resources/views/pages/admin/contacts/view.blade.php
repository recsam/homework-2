@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact View')

@section('body')
    <div class="d-flex justify-content-end mb-2">
        <button class="ml-1 btn btn-sm btn-outline-secondary">Pending</button>
        <button class="ml-1 btn btn-sm btn-outline-secondary">In Progress</button>
        <button class="ml-1 btn btn-sm btn-outline-secondary">Completed</button>
    </div>
    @include('lists.viewcontact')
    <div class="d-flex justify-content-between">
        <a href="{{ route('admin.contacts.index') }}" class="btn btn-outline-secondary"><i class="fa fa-chevron-left"></i> Back</a>
        <button type="button" id="btnReply" class="btn btn-outline-secondary">Reply</button>
    </div>
    @include('modals.reply')
    @push('script')
        <script>
            $(function () {
                $('#btnReply').on('click', function (event) {
                    $('#reply-modal').modal('show');
                })
            })
        </script>
    @endpush
@stop

