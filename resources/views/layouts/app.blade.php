<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>@yield('title')</title>
</head>
<body class="container">
    <h2 class="text-uppercase">@yield('pageTitle')</h2>
    <hr class="mb-4">
    @yield('body')


    <script src="{{ mix('js/app.js') }}"></script>
    @stack('script')
</body>
</html>
