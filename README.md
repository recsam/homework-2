# Homework 2

### Layout
- The views divide into fragments and to insert back fragments, `@include` is used.
- All of the views use `@extends` to extend from the `app.blade.php` in the directory `layouts`

### Component
The bulk email and email modal both utilizes the formmodal component which is a component for forms.

### Stack
There is a script stack in `app.blade.php` which the views can extend in order to append javascript code to the end of the page (This is used to that the code can understand the codes from bootstrap and jquery).
