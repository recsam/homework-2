<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/contacts');

Route::group([
    'prefix' => '/contacts',
    'as' => 'contacts.'
], function () {
    Route::get('/', 'ContactController@create')->name('create');
    Route::get('/confirm', 'ContactController@confirm')->name('confirm');
    Route::get('/success', 'ContactController@success')->name('success');
});

Route::group([
    'prefix' => '/admin',
    'as' => 'admin.'
], function () {
    Route::group([
        'prefix' => '/contacts',
        'as' => 'contacts.'
    ], function () {
        Route::get('/', 'AdminContactController@index')->name('index');
        Route::get('/{id}', 'AdminContactController@view')->name('success');
    });
});

